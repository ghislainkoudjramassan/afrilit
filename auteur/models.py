from django.db import models

# Create your models here.
SEXE = {
    ('feminin','Féminin'),
    ('masculin','Masculin'),

}

class Auteur(models.Model):
    nom = models.CharField(max_length=100, verbose_name="nom")
    prenom = models.CharField(max_length=100, verbose_name="prenom", blank=True)
    sexe = models.CharField(max_length=100, verbose_name="sexe", choices=SEXE)

    def __str__(self):
        return self.nom
