# Generated by Django 3.1.3 on 2020-11-08 16:04

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('oeuvre', '0002_auto_20201108_1600'),
    ]

    operations = [
        migrations.AlterField(
            model_name='oeuvre',
            name='description',
            field=models.TextField(blank=True, verbose_name='description'),
        ),
        migrations.AlterField(
            model_name='oeuvre',
            name='isbn',
            field=models.IntegerField(blank=True),
        ),
        migrations.AlterField(
            model_name='oeuvre',
            name='titre',
            field=models.CharField(blank=True, max_length=100, verbose_name='nom'),
        ),
    ]
