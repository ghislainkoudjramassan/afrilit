from django.contrib import admin
from oeuvre.models import (
    Oeuvre
)
# Register your models here.

@admin.register(Oeuvre)
class OeuvreAdmin(admin.ModelAdmin):
    pass