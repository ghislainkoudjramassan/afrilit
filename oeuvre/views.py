from django.shortcuts import render
from oeuvre.models import Oeuvre

# Create your views here.

def page2(request):
    return render(request, 'oeuvre/oeuvre.html')

def liste(request):
    oeuvre = Oeuvre.objects.all()
    return render(request, 'oeuvre/oeuvre.html', {'liste':oeuvre})

def oeuvreDetails(request):
    return render(request, 'oeuvre/oeuvre.html')